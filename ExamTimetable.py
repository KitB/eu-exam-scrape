from BeautifulSoup import BeautifulSoup
import urllib2
import re
import sys
import icalendar
from datetime import datetime


class ExamTable(object):
    titleRE = re.compile("(.*) - (.*)")
    dateRE = re.compile("(.*), (.*)")
    timeRE = re.compile("(\d\d:\d\d):\d\d-(\d\d:\d\d):\d\d")

    def __init__(self, soup):
        tds = soup.findAll('td')
        title = tds[0].getText()
        m = self.titleRE.match(title)
        self.code = m.group(1)
        self.name = m.group(2)
        self.school = tds[1].getText()
        self.location = tds[2].find('strong').getText()
        date = tds[3].find('strong').getText()
        dm = self.dateRE.match(date)
        dt = datetime.strptime(dm.group(1), "%A %d/%m/%Y")
        tm = self.timeRE.match(dm.group(2))
        if tm:
            self.starttime = datetime.combine(
                dt, datetime.strptime(tm.group(1), "%H:%M").time())
            self.endtime = datetime.combine(
                dt, datetime.strptime(tm.group(2), "%H:%M").time())
        else:
            self.starttime = self.endtime = dt
        self.date = dt.strftime("%d %B")

    def toEvent(self):
        e = icalendar.Event()
        e.add('summary', self.name)
        e.add('dtstart', self.starttime)
        e.add('dtend', self.endtime)
        e.add('dtstamp', self.starttime)
        e.add('location', self.location)
        return e

    def __unicode__(self):
        return u"Exam \"{name}\" occurs on {date} between {st} and {et}".format(name=self.name, date=self.date, st=self.starttime, et=self.endtime)

data = {'searchfrm': 'yes',
        'school': 'School of Informatics',
        'code': '',
        'course': ''
        }
import copy
values = copy.deepcopy(data)
import urllib
data = urllib.urlencode(values)
usock = urllib2.urlopen("http://www.scripts.sasg.ed.ac.uk/registry/examinations/index3.cfm", data)
b = BeautifulSoup(usock.read())
cA = b.find('div', attrs={'id': 'contentArea'})
examTables = cA.findAll('table', attrs={'width': '100%', 'border': '0', 'cellspacing': '0', 'cellpadding': '0'})
examTables = map(ExamTable, examTables)
cal = icalendar.Calendar()
for e in examTables:
    print "%s\n" % unicode(e)
    cal.add_component(e.toEvent())

file = sys.argv[1]
f = open(file, 'wb')
f.write(cal.to_ical())
f.close()
